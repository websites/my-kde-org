from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpRequest
from django.views.generic import FormView, UpdateView, TemplateView, View
from django.shortcuts import redirect
from django.contrib import messages

from .. import forms, models


class DeveloperRequestView(LoginRequiredMixin, FormView):
    template_name = "bid_main/developer_request.html"
    form_class = forms.DeveloperAccessRequestForm
    success_url = "/"

    def dispatch(self, request: HttpRequest, *args, **kwargs):
        if models.DeveloperAccessRequest.objects.filter(user=request.user):
            return redirect("/developer-access-pending")
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form: forms.DeveloperAccessRequestVerificationForm):
        request_model = form.save(commit=False)
        request_model.user = self.request.user
        request_model.save()
        return super().form_valid(form)


class DeveloperRequestPending(LoginRequiredMixin, TemplateView):
    template_name = "bid_main/developer_request_pending.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["developer_request"] = self.developer_request
        return context

    def dispatch(self, request: HttpRequest, *args, **kwargs):
        try:
            self.developer_request = models.DeveloperAccessRequest.objects.get(
                user=request.user
            )
            return super().dispatch(request, *args, **kwargs)
        except models.DeveloperAccessRequest.DoesNotExist:
            return redirect("/developer-access-request")


class DeveloperRequestCancelView(LoginRequiredMixin, View):
    def dispatch(self, request: HttpRequest, *args, **kwargs):
        models.DeveloperAccessRequest.objects.filter(user=request.user).delete()
        return redirect("/")


class DeveloperRequestVerificationView(LoginRequiredMixin, UpdateView):
    template_name = "bid_main/developer_request_verification.html"
    fields = ["sponsor_agreed"]
    model = models.DeveloperAccessRequest
    success_url = "/"

    def dispatch(self, request: HttpRequest, *args, **kwargs):
        request_model = models.DeveloperAccessRequest.objects.filter(
            pk=kwargs.get("pk")
        )
        if not request_model.exists():
            messages.add_message(
                request,
                messages.INFO,
                "Request not found. Probably already accepted or refused.",
            )
            return redirect("/")
        if request.user != request_model[0].sponsor:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form: forms.DeveloperAccessRequestVerificationForm):
        request_model = form.save(commit=False)
        request_model.user = self.request.user
        request_model.save()
        return super().form_valid(form)
