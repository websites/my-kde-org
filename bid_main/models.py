import typing
import uuid
import pytz

from django import urls
from django.db import models
from django.core import validators
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.utils import timezone
from django.utils.deconstruct import deconstructible
from django.utils.translation import gettext_lazy as _
import sorl.thumbnail
from django.db.models.signals import post_save, post_init
from django.dispatch import receiver
from django.conf import settings
from martor.models import MartorField

import oauth2_provider.models as oa2_models
from . import fields


class DeveloperAccessRequest(models.Model):
    """
    Model representing a developer access request
    """

    user = models.OneToOneField(
        "User",
        on_delete=models.CASCADE,
        related_name="developer_request",
    )
    reason = models.TextField()
    sponsor = models.ForeignKey(
        "User", on_delete=models.CASCADE, related_name="developer_request_sponsor"
    )

    AGREED = "AG"
    REFUSED = "RE"
    NOT_DECIDED = "ND"

    SPONSOR_CHOICES = [
        (AGREED, "Agree"),
        (REFUSED, "Refuse"),
        (NOT_DECIDED, "Not decided"),
    ]

    sponsor_agreed = models.CharField(
        choices=SPONSOR_CHOICES, default=NOT_DECIDED, max_length=2
    )

    @property
    def sponsor_choice(self):
        for c in self.SPONSOR_CHOICES:
            if c[0] == self.sponsor_agreed:
                return c[1]


class GitlabGroup(models.Model):
    """
    Model representinmodels.g a gitlab group
    """

    gitlab_group_id = models.IntegerField()
    name = models.CharField(max_length=256)
    web_url = models.CharField(max_length=256)

    def __str__(self):
        return "%s" % self.name


class RoleManager(models.Manager):
    def badges(self) -> models.QuerySet:
        """Query for those roles that are considered badges."""

        return (
            self.filter(is_public=True, is_active=True, is_badge=True)
            .exclude(badge_img__isnull=True)
            .exclude(badge_img="")
        )


class Role(models.Model):
    name = models.CharField(max_length=80)
    description = models.CharField(
        max_length=255,
        blank=True,
        null=False,
        help_text="Note that this is shown for badges on users' dashboard page.",
    )
    is_active = models.BooleanField(default=True, null=False)
    is_badge = models.BooleanField(
        default=False,
        null=False,
        help_text="Note that a roles is only actually used as a badge when this checkbox "
        "is enabled <strong>and</strong> it has a badge image.",
    )
    is_public = models.BooleanField(
        default=True,
        null=False,
        help_text="When enabled, this role/badge will be readable through the userinfo API.",
    )

    may_manage_roles = models.ManyToManyField(
        "Role",
        related_name="managers",
        blank=True,
        help_text="Users with this role will be able to grant or revoke these roles to "
        "any other user.",
    )

    # For Badges:
    label = models.CharField(
        max_length=255,
        blank=True,
        null=False,
        help_text="Human-readable name for a badge. Required for badges, not for roles.",
    )
    badge_img = sorl.thumbnail.ImageField(
        verbose_name="Badge image",
        help_text="Visual representation of a badge.",
        upload_to="badges",
        height_field="badge_img_height",
        width_field="badge_img_width",
        null=True,
        blank=True,
    )
    badge_img_height = models.IntegerField(null=True, blank=True)
    badge_img_width = models.IntegerField(null=True, blank=True)
    link = models.URLField(
        null=True,
        blank=True,
        help_text="Clicking on a badge image will lead to this link.",
    )

    # for gitlab authorization
    gitlab_group = models.ManyToManyField(GitlabGroup, related_name="roles", blank=True)

    objects = RoleManager()

    class Meta:
        ordering = ["-is_active", "name"]

    def __str__(self):
        if self.is_active:
            return self.name
        return "%s [inactive]" % self.name

    @property
    def admin_url(self) -> str:
        view_name = f"admin:{self._meta.app_label}_{self._meta.model_name}_change"
        return urls.reverse(view_name, args=(self.id,))

    def clean(self):
        # Labels are required for badges.
        if self.is_badge and not self.label:
            raise ValidationError({"label": _("Badges must have a label.")})


@receiver(post_save, sender=Role)
def update_gitlab_permissions(sender, **kwargs):
    return
    import requests

    user = kwargs.get("instance")
    GITLAB_URL = "http://gitlab.localhost"
    API_VERSION = "v4"
    headers = {"PRIVATE-TOKEN": "cyHrgEVCGu-XySMg7r1Q", "Sudo": "root"}

    # make sure use with nickname and uid really exist
    user_gitlab = requests.get(
        GITLAB_URL + "/api/" + API_VERSION + "/groups", headers=headers
    ).json()
    print(user_gitlab)

    user_gitlab = requests.get(
        GITLAB_URL
        + "/api/"
        + API_VERSION
        + "/users?extern_uid="
        + str(user.uuid)
        + "&provider=KDEIdentity",
        headers=headers,
    ).json()
    if len(user_gitlab) == 0:
        print("Sender with  uuid doesn't exist " + str(user.uuid))
        return
    user_gitlab = user_gitlab[0]

    user_id = user_gitlab["id"]

    for gitlab_group in user.gitlab_group.all():
        print(
            "trying to grant group access to "
            + str(gitlab_group.gitlab_group_id)
            + " to user "
            + str(user_id)
        )
        r = requests.post(
            GITLAB_URL
            + "/api/"
            + API_VERSION
            + "/groups/"
            + str(gitlab_group.gitlab_group_id)
            + "/members",
            data={"user_id": user_id, "access_level": "30"},
            headers=headers,
        )
        print(r.json())


class UserManager(BaseUserManager):
    """UserManager that doesn't use a username, but an email instead."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)


@deconstructible
class UnicodeNicknameValidator(validators.RegexValidator):
    regex = r"^[\w.+-]+$"
    message = _(
        "Enter a valid nickname. This value may contain only letters, "
        "numbers, and ./+/-/_ characters."
    )


class MastodonValidator(validators.RegexValidator):
    regex = r"^@[^@]*@[^@]*$"
    message = "Enter a mastodon nickname in the form @name@server"


class User(AbstractBaseUser, PermissionsMixin):
    """
    User class for BlenderID, implementing a fully featured User model with
    admin-compliant permissions.


    Email and password are required. Other fields are optional.
    """

    uuid = models.UUIDField(default=uuid.uuid4, editable=False)

    email = models.EmailField(
        _("email address"),
        max_length=64,
        unique=True,
        help_text=_("Required. 64 characters or fewer."),
        error_messages={
            "unique": _("A user with that email address already exists."),
        },
        db_index=True,
    )
    full_name = models.CharField(
        _("Full Name"), max_length=80, blank=False, db_index=True
    )

    # Named 'nickname' and not 'username', because the latter already is a
    # 'thing' in Django. Not having one, and identifying users by their email
    # address, was our own customisation.
    # Bringing back a field 'username' with different semantics would be
    # confusing. For example, there still is a field 'username' in the default
    # Django auth form that is used for 'the thing used to log in', which is
    # the email address in our case.
    nickname = models.CharField(
        "nickname",
        max_length=80,
        unique=True,
        help_text=_(
            "A short (one-word) name that can be used to address you. "
            "80 characters or fewer. Letters, digits, and ./+/-/_ only."
        ),
        validators=[UnicodeNicknameValidator()],
        error_messages={
            "unique": _("That name is already used by someone else."),
        },
    )

    avatar = fields.AvatarField(upload_to="user-avatars", null=True, blank=True)

    roles = models.ManyToManyField(Role, related_name="users", blank=True)

    public_roles_as_string = models.CharField(
        "Public roles as string",
        max_length=255,
        blank=True,
        default="",
        help_text=_(
            "String representation of public roles, for comparison in webhooks"
        ),
    )
    private_badges = models.ManyToManyField(
        Role,
        blank=True,
        limit_choices_to={"is_badge": True},
        help_text='Badges marked as "private" by the user. Can contain badges '
        "they do not have at the moment; this is fine.",
    )

    confirmed_email_at = models.DateTimeField(
        null=True,
        blank=True,
        help_text=_(
            "Designates the date & time at which the user confirmed their email address. "
            "None if not yet confirmed."
        ),
    )
    last_login_ip = models.GenericIPAddressField(
        null=True,
        blank=True,
        help_text=_("IP address (IPv4 or IPv6) used for previous login, if any."),
    )
    current_login_ip = models.GenericIPAddressField(
        null=True,
        blank=True,
        help_text=_("IP address (IPv4 or IPv6) used for current login, if any."),
    )
    login_count = models.PositiveIntegerField(
        default=0, blank=True, help_text=_("Number of times this user logged in.")
    )

    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    date_deletion_requested = models.DateTimeField(
        null=True,
        blank=True,
        help_text=_(
            "Indicates when deletion of this account was requested, if ever."
            " Once set, it should not be changed. Can be set on the users list."
        ),
    )

    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    last_update = models.DateTimeField(_("last update"), default=timezone.now)
    privacy_policy_agreed = models.DateTimeField(
        _("privacy policy agreed"),
        null=True,
        blank=True,
        help_text=_("Date when this user agreed to our privacy policy."),
    )

    email_change_preconfirm = models.EmailField(
        _("email address to change to"),
        blank=True,
        max_length=64,
        help_text=_("New address for the user, set while in the confirmation flow."),
    )

    objects = UserManager()

    public_profile = models.BooleanField(
        default=False,
        help_text=_("Indicate if the profile is visible to not logged in users"),
    )

    bio = MartorField(
        blank=True,
        help_text=_("Public information about the user"),
        verbose_name="Description",
    )

    homepage = models.CharField(
        max_length=200,
        blank=True,
        help_text=_("The homepage of the user"),
        verbose_name="Homepage",
    )

    TIMEZONE_CHOICES = zip(pytz.all_timezones, pytz.all_timezones)

    timezone = models.CharField(
        blank=True,
        help_text=_("The time zone of the user"),
        max_length=200,
        choices=TIMEZONE_CHOICES,
        verbose_name="Timezone",
    )

    matrix_nick = models.CharField(
        blank=True, max_length=80, verbose_name="Matrix Username"
    )

    irc_nick = models.CharField(
        blank=True, max_length=80, verbose_name="IRC/Libera Username"
    )

    mastodon_nick = models.CharField(
        blank=True,
        max_length=200,
        verbose_name="Mastodon Username",
        validators=[MastodonValidator()],
    )

    twitter_nick = models.CharField(
        blank=True, max_length=200, verbose_name="Twitter Username"
    )

    liberapay_nick = models.CharField(
        blank=True, max_length=200, verbose_name="Liberapay Username"
    )

    @property
    def twitter_nick_link(self):
        if self.twitter_nick == "":
            return ""
        elif self.twitter_nick.startswith("@"):
            return self.twitter_nick[1:]
        elif self.twitter_nick.startswith("http"):
            return self.twitter_nick.split("/")[-1]
        return self.twitter_nick

    @property
    def mastodon_link(self):
        if self.mastodon_nick == "":
            return ""
        s = self.mastodon_nick.split("@")
        if len(s) != 3:
            return "Error in mastodon link"
        return "https://" + s[2] + "/@" + s[1]

    @property
    def had_social_nick(self):
        return (
            self.mastodon_nick != ""
            or self.twitter_nick != ""
            or self.irc_nick != ""
            or self.matrix_nick != ""
        )

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def __repr__(self) -> str:
        return f"<User id={self.id} email={self.email!r}>"

    def save(self, *args, **kwargs):
        self.last_update = timezone.now()
        updated_fields = {"last_update"}

        if kwargs.get("update_fields") is not None:
            kwargs["update_fields"] = set(kwargs["update_fields"]).union(updated_fields)

        return super().save(*args, **kwargs)

    def public_roles(self) -> set:
        """Returns public role names.

        Used in the bid_api.signals module to detect role changes without
        using more lookups in the database.
        """
        return {role.name for role in self.roles.filter(is_public=True, is_active=True)}

    def public_badges(self) -> models.query.QuerySet:
        """Returns a QuerySet of public badges.

        Public badges are those badges that are marked as public in the database
        and not marked private by the user.
        """
        private_badge_ids = {role.id for role in self.private_badges.all()}
        return self.all_badges().exclude(id__in=private_badge_ids)

    def all_badges(self) -> models.query.QuerySet:
        """Returns a QuerySet of all badges.

        Returned are those badges that are marked as public in the database,
        regardless of which ones the user marked as private.
        """
        return self.roles.badges()

    def get_full_name(self):
        """
        Returns the full name.
        """
        return self.full_name.strip()

    def get_short_name(self):
        """Returns the short name for the user."""
        parts = self.full_name.split(" ", 1)
        return parts[0]

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    @property
    def has_confirmed_email(self):
        return self.confirmed_email_at is not None

    @property
    def email_to_confirm(self):
        """The email to confirm, either 'email_change_preconfirm' when set, or 'email'."""
        return self.email_change_preconfirm or self.email

    @property
    def role_names(self) -> typing.Set[str]:
        return {role.name for role in self.roles.all()}

    @property
    def must_pp_agree(self) -> bool:
        """Return True when user needs to agree to new privacy policy."""
        return (
            self.privacy_policy_agreed is None
            or self.privacy_policy_agreed < settings.PPDATE
        )


class SettingValueField(models.CharField):
    def __init__(self, *args, **kwargs):
        kwargs["max_length"] = 128
        super().__init__(*args, **kwargs)


SETTING_DATA_TYPE_CHOICES = [
    ("bool", "Boolean"),
]


class Setting(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=128)
    data_type = models.CharField(
        max_length=32, choices=SETTING_DATA_TYPE_CHOICES, default="bool"
    )
    default = SettingValueField()
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, through="UserSetting")

    def __str__(self):
        return self.name.replace("_", " ").title()


class UserSetting(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    setting = models.ForeignKey(Setting, on_delete=models.CASCADE)
    unconstrained_value = SettingValueField()

    def __str__(self):
        return "Setting %r of %r" % (self.setting.name, self.user.email)


class OAuth2AccessToken(oa2_models.AbstractAccessToken):
    class Meta:
        verbose_name = "OAuth2 access token"

    host_label = models.CharField(max_length=255, unique=False, blank=True)
    subclient = models.CharField(max_length=255, unique=False, blank=True)


class OAuth2RefreshToken(oa2_models.AbstractRefreshToken):
    class Meta:
        verbose_name = "OAuth2 refresh token"


class OAuth2Application(oa2_models.AbstractApplication):
    class Meta:
        verbose_name = "OAuth2 application"

    url = models.URLField(
        unique=False, blank=True, help_text="URL to display on 'access' page for users."
    )
    notes = models.TextField(
        blank=True,
        help_text="Information about this application, for staff only, not to present on frontend.",
    )

    authorized_roles = models.ManyToManyField(
        Role, related_name="authorized_roles", blank=True
    )


class UserNote(models.Model):
    """CRM-like note added to a user."""

    class Meta:
        verbose_name = "Note"
        ordering = ("-created",)

    user = models.ForeignKey(User, related_name="notes", on_delete=models.CASCADE)
    creator = models.ForeignKey(
        User,
        null=True,
        blank=True,
        related_name="created_notes",
        on_delete=models.PROTECT,
        limit_choices_to={"is_staff": True},
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    note = models.TextField(blank=False)

    def __str__(self):
        return "Note"


class SecondaryEmail(models.Model):
    """
    Model used to store secondary emails. This is used as a backup way to contact the users in case, they are not
    responding to their primary address.

    To confirm that the email is correct, a email will be send to the given email address with a generated token. The
    user will need to confirm the email by sending the token to the server.
    """

    user = models.ForeignKey(
        User, related_name="secondaryEmails", on_delete=models.CASCADE
    )
    email = models.EmailField()
    token = models.CharField(
        "token",
        max_length=255,
        unique=True,
        null=True,
        help_text=_("Token send to the email address and will be verified"),
    )

    @property
    def is_confirmed(self):
        return self.token is None

    def __str__(self) -> str:
        return self.email
