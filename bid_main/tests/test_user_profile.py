from pathlib import Path
import tempfile

from django.test import TransactionTestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile

class AutoCreateUserProfileTest(TransactionTestCase):
    def test_create_user_happy_flow(self):
        user_cls = get_user_model()
        user = user_cls(email="example@example.com", full_name="Dr. Examplović")
        user.save()

        self.assertEqual("Dr. Examplović", user.full_name)
        return user


class ProfileEditTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        user_cls = get_user_model()
        cls.user = user_cls(email="example@example.com", full_name="Dr. Examplović")
        cls.user.save()

        my_dir = Path(__file__).absolute().parent
        avatar_path = my_dir / "media/user-avatars/test-avatar.jpg"
        with avatar_path.open("rb") as infile:
            cls.avatar_bytes = infile.read()

        cls.fake_media_tempdir = tempfile.TemporaryDirectory()
        cls.fake_media_root = Path(cls.fake_media_tempdir.name)

    def setUp(self):
        super().setUp()
        self.settings_modifier = self.settings(
            MEDIA_ROOT=self.fake_media_root,
        )

    @classmethod
    def tearDownClass(cls):
        fake_media_tempdir = getattr(cls, "fake_media_tempdir", None)
        if fake_media_tempdir:
            fake_media_tempdir.cleanup()
        super().tearDownClass()

    def test_edit_profile_and_avatar(self):
        """Test setting avatar, then changing details without submitting another avatar."""

        from bid_main.forms import UserProfileForm

        form_data = {
            "full_name": self.user.full_name,
            "email": self.user.email,
            "nickname": "Examplović",
        }
        file_data = {"avatar": SimpleUploadedFile("face.jpg", self.avatar_bytes)}

        with self.settings_modifier:
            form = UserProfileForm(form_data, file_data, instance=self.user)
            self.assertTrue(form.is_valid(), msg=f"Errors: {form.errors}")
            form.save()
            self.user.refresh_from_db()

            thumb_path = self.fake_media_root / self.user.avatar.thumbnail_path()
            uploaded_file_path = self.fake_media_root / self.user.avatar.name

        # Check the thumbnail cache.
        self.assertTrue(thumb_path.exists(), f"{thumb_path} should exist")
        self.assertTrue(
            uploaded_file_path.exists(), f"{uploaded_file_path} should exist"
        )

        # Delete the avatar from disk. This shouldn't prevent subsequent form posts.
        uploaded_file_path.unlink()

        form_data = {
            "full_name": "New Name",
            "email": self.user.email,
            "nickname": "Examplović",
        }
        # Save the form without uploading an avatar.
        file_data = {"avatar": None}
        with self.settings_modifier:
            form = UserProfileForm(form_data, file_data, instance=self.user)
            self.assertTrue(form.is_valid(), msg=f"Errors: {form.errors}")
            form.save()
            self.user.refresh_from_db()

        self.assertEqual("New Name", self.user.full_name)
