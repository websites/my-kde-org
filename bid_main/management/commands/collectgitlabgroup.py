import requests

from django.core.management import CommandError, BaseCommand
from django.db import models

from bid_main.models import GitlabGroup


GITLAB_URL = "http://gitlab.localhost"
API_VERSION = "v4"


class Command(BaseCommand):
    """
    Command to import gitlab group into the GitlabGroup model
    """

    def handle(self, *args, **options):
        r = requests.get(GITLAB_URL + "/api/" + API_VERSION + "/groups")
        group_added_count = 0
        for group in r.json():
            if len(GitlabGroup.objects.filter(gitlab_group_id=group["id"])) == 0:
                # new group
                new_group = GitlabGroup(
                    gitlab_group_id=group["id"],
                    name=group["name"],
                    web_url=group["web_url"],
                )
                new_group.save()
                self.stdout.write("New group added " + group["name"])

        self.stdout.write("New group added count: " + str(group_added_count))
