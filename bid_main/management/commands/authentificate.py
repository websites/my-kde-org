# SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
#
# SPDX-LicenseRef: AGPL-3.0-or-later

from django.conf import settings
from django.core.management import CommandError, BaseCommand
from django.contrib.auth import authenticate
import sys


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--username")
        parser.add_argument("--password")

    def handle(self, *args, **options):
        user = authenticate(username=options["username"], password=options["password"])

        if user is None:
            raise CommandError("User not found")
