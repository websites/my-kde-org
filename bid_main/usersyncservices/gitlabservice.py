from .abstractusersyncservice import AbstractUserSyncService
from .. import models
import requests
from typing import Dict


class GitlabService(AbstractUserSyncService):
    API_VERSION = "v4"
    DEVELOPER_ACCESS_LEVEL = "30"

    def __init__(
        self, url: str, name: str, private_token: str, authorizations: Dict[str, int]
    ):
        super().__init__(self, url, name)

        self.authorizations = authorizations
        self.headers = {"PRIVATE-TOKEN": self.private_token, "Sudo": "root"}

    def user_exist() -> bool:
        """This method should test if the user exist on the service. If the user
        doesn't exist this method will do nothing."""

        user_gitlab = requests.get(
            "{}/api/{}/users?extern_uid={}&provider=MyKDE".format(
                self.url, self.API_VERSION, str(user.uuid)
            ),
            headers=headers,
        ).json()

        if len(user_gitlab) == 0:
            return False

        # save the user for later usage
        assert len(user_gitlab) == 1
        self.user_gitlab = user_gitlab[0]

        return True

    def update_profile(self, user: models.User) -> None:
        """This method should be responsible for updating the user profile on the
        service and can assume the user exist."""

        user_id = self.user_gitlab["id"]

        # TODO remove membership from group
        # but needs to figure out first the team part

        for role in user.roles:
            if role.name in authorizations:
                requests.post(
                    "{}/api/{}/groups/{}/members".format(
                        self.url, self.API_VERSION, str(authorizations[role.name])
                    ),
                    data={
                        "user_id": user_id,
                        "access_level": self.DEVELOPER_ACCESS_LEVEL,
                    },
                    headers=self.headers,
                )

        # TODO sync fullname, and other metadata
